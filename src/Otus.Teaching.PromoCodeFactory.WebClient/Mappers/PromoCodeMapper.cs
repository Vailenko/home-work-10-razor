﻿﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebClient.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebClient.Mappers
{
    public class PromoCodeMapper
    {
        public static PromoCode MapFromModel(GivePromoCodeRequest request, Preference preference, IEnumerable<Customer> customers) {

            var promocode = new PromoCode();
            promocode.Id = Guid.NewGuid();
            
            promocode.PartnerName = request.PartnerName;
            promocode.Code = request.PromoCode;
            promocode.ServiceInfo = request.ServiceInfo;
           
            promocode.BeginDate = DateTime.Now;
            promocode.EndDate = DateTime.Now.AddDays(30);

            promocode.Preference = preference;
            promocode.PreferenceId = preference.Id;

            promocode.Customers = new List<PromoCodeCustomer>();

            foreach (var item in customers)
            {
                promocode.Customers.Add(new PromoCodeCustomer()
                {

                    CustomerId = item.Id,
                    Customer = item,
                    PromoCodeId = promocode.Id,
                    PromoCode = promocode
                });
            };

            return promocode;
        }

        public static PromoCode MapFromModel(PromoCodeEditRequest request, PromoCode promocode)
        {            
            promocode.Id = request.Id;
            promocode.PartnerName = request.PartnerName;
            promocode.Code = request.Code;
            promocode.ServiceInfo = request.ServiceInfo;
            promocode.BeginDate = DateTime.Parse(request.BeginDate);
            promocode.EndDate = DateTime.Parse(request.EndDate);

            return promocode;
        }
        public static PromoCodeDetailResponse MapToModel(PromoCode promoCode)
        {

            var response = new PromoCodeDetailResponse();
            response.BeginDate = promoCode.BeginDate.ToString("yyyy-MM-dd");
            response.Code = promoCode.Code;
            response.EndDate = promoCode.EndDate.ToString("yyyy-MM-dd");
            response.PartnerName = promoCode.PartnerName;
            response.PreferenceName = promoCode.Preference.Name;
            response.CustomerNames = string.Join(",", promoCode.Customers.Select(x => x.Customer.LastName).ToList());            
            return response;            
        }

        public static GivePromoCodeRequest MapToGiveModel(PromoCode promoCode)
        {

            var response = new GivePromoCodeRequest();            
            response.PartnerName = promoCode.PartnerName;
            response.Preference = promoCode.Preference.Name;
            response.PromoCode = promoCode.Code;
            response.ServiceInfo = promoCode.ServiceInfo;
            return response;
        }
    }
}
