﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebClient.Mappers;
using Otus.Teaching.PromoCodeFactory.WebClient.Models;



namespace Otus.Teaching.PromoCodeFactory.WebClient.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>

    public class PromocodesController
        : Controller
    {
        private readonly IRepository<PromoCode> _promoCodesRepository;
        private readonly IRepository<Preference> _preferencesRepository;
        private readonly IRepository<Customer> _customersRepository;

        public PromocodesController(IRepository<PromoCode> promoCodesRepository, 
            IRepository<Preference> preferencesRepository, IRepository<Customer> customersRepository)
        {
            _promoCodesRepository = promoCodesRepository;
            _preferencesRepository = preferencesRepository;
            _customersRepository = customersRepository;
        }

        /// <summary>
        /// Список Промокодов
        /// </summary>
        /// <returns></returns>
        public ActionResult List()
        {
            var promocodes = _promoCodesRepository.GetAllAsync();

            var response = promocodes.Result.Select(x => new PromoCodeShortResponse()
            {
                Id = x.Id,
                Code = x.Code,
                PartnerName = x.PartnerName,
                BeginDate = x.BeginDate.ToString("yyyy-MM-dd"),
                EndDate = x.EndDate.ToString("yyyy-MM-dd"),               
                
            }).ToList();

            return View(response);
        }

        /// <summary>
        /// Для деталей
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> Details(Guid id)
        {

            var promoCode = await _promoCodesRepository.GetByIdAsync(id);

            //var promoCode = await _context.PromoCodes
            //    .Include(p => p.Customer)
            //    .FirstOrDefaultAsync(m => m.Id == id);
            if (promoCode == null)
            {
                return NotFound();
            }

            var response = PromoCodeMapper.MapToModel(promoCode);
            return View(response);

        }

        /// <summary>
        /// Для редактирования
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> Edit(Guid id)
        {
            return await Details(id);
        }

        /// <summary>
        /// Для подтверждения удаления
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> Delete(Guid id)
        {

            return await Details(id);
        }

        /// <summary>
        /// Для Создания
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> Create()
        {

            return View();

        }


        /// <summary>
        /// Изменить промокод
        /// </summary>
        /// <returns></returns>
        [HttpPost, ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditAction(Guid id, PromoCodeEditRequest request)
        {
            
            var promoCode = await _promoCodesRepository.GetByIdAsync(id);

            if (promoCode == null)
            {
                return NotFound();
            }


            var updatedPromoCode = PromoCodeMapper.MapFromModel(request, promoCode);

            if (ModelState.IsValid)
            {
                await _promoCodesRepository.UpdateAsync(updatedPromoCode);
                return RedirectToAction("list");
            }
            var response = PromoCodeMapper.MapToModel(updatedPromoCode);
            return View(response);
        }

        /// <summary>
        /// Удалить промокод
        /// </summary>
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteAction(Guid id)
        {
            var promoCode = await _promoCodesRepository.GetByIdAsync(id);
            if (promoCode == null)
            {
                return NotFound();
            }
            await _promoCodesRepository.DeleteAsync(promoCode);
            return RedirectToAction("List");
        }

        /// <summary>
        /// Создать промокод
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(GivePromoCodeRequest request)
        {

            var preference = await _preferencesRepository.GetFirstWhere(x => x.Name == request.Preference);

            if (preference == null)
            {
                return BadRequest();
            }

            //  Получаем клиентов с этим предпочтением:
            var customers = await _customersRepository
                .GetWhere(d => d.Preferences.Any(x =>
                    x.Preference.Id == preference.Id));

            PromoCode promoCode = PromoCodeMapper.MapFromModel(request, preference, customers);

            if (ModelState.IsValid)
            {
                await _promoCodesRepository.AddAsync(promoCode);
                return RedirectToAction("List");

            }                        
            return View(request);
        }


    
    }
}