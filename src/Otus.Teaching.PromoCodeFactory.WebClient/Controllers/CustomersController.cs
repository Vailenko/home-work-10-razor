﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebClient.Mappers;
using Otus.Teaching.PromoCodeFactory.WebClient.Models;

namespace Otus.Teaching.PromoCodeFactory.WebClient.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>

    public class CustomersController
        : Controller
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;

        public CustomersController(IRepository<Customer> customerRepository, 
            IRepository<Preference> preferenceRepository)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
        }




        /// <summary>
        /// Список Покупателей
        /// </summary>
        /// <returns></returns>
        public async Task<ActionResult> List()
        {
                        

            var customers = await _customerRepository.GetAllAsync();

            var response = customers.Select(x => new CustomerShortResponse()
            {
                Id = x.Id,
                Email = x.Email,
                FirstName = x.FirstName,
                LastName = x.LastName
            }).ToList();

            return View(response);
        }

        /// <summary>
        /// Для деталей
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> Details(Guid id)
        {

            var customer = await _customerRepository.GetByIdAsync(id);

            if (customer == null)
            {
                return NotFound();
            }

            var response = new CustomerResponse (customer);
            return View(response);

        }

        /// <summary>
        /// Для редактирования
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> Edit(Guid id)
        {

            var customer = await _customerRepository.GetByIdAsync(id);

            if (customer == null)
            {
                return NotFound();
            }

            var response = new CreateOrEditCustomerRequest (customer);
            return View(response);

        }

        /// <summary>
        /// Для подтверждения удаления
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> Delete(Guid id)
        {

            return await Details(id);
        }

        /// <summary>
        /// Для Создания
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> Create()
        {

            return View();

        }


        /// <summary>
        /// Создать покупателя
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult<CustomerResponse>> Create(CreateOrEditCustomerRequest request)
        {
            
            var preferences = new List<Preference>();
            var preferenceNames = request.Preferences.Split(',').ToList();
            foreach (var preferenceName in preferenceNames)
            {
                var preference = await _preferenceRepository.GetFirstWhere(s=>s.Name == preferenceName);
                if (preference != null)
                    preferences.Add(preference);
            }
                


            Customer customer = CustomerMapper.MapFromModel(request, preferences);


            if (ModelState.IsValid)
            {
                await _customerRepository.AddAsync(customer);
                return RedirectToAction("List");

            }
            return View(request);
            
        }


        /// <summary>
        /// Изменить покупателя
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]        
        public async Task<IActionResult> Edit(Guid id, CreateOrEditCustomerRequest request)
        {
            var customer = await _customerRepository.GetByIdAsync(id);
            
            if (customer == null)
                return NotFound();

            var preferences = new List<Preference>();
            var preferenceNames = request.Preferences.Split(',').ToList();
            foreach (var preferenceName in preferenceNames)
            {
                var preference = await _preferenceRepository.GetFirstWhere(s => s.Name == preferenceName);
                if (preference != null)
                    preferences.Add(preference);
            }


            CustomerMapper.MapFromModel(request, preferences, customer);
            
            if (ModelState.IsValid)
            {
                await _customerRepository.UpdateAsync(customer);
                return RedirectToAction("List");

            }
            return View(request);                       
        }




        /// <summary>
        /// Удалить покупателя
        /// </summary>
        /// <returns></returns>
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        
        public async Task<IActionResult> ActionDelete(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);
            
            if (customer == null)
                return NotFound();

            await _customerRepository.DeleteAsync(customer);
            return RedirectToAction("List");
 
        }
    }
}