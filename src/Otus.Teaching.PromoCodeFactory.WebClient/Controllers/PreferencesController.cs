﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebClient.Models;

namespace Otus.Teaching.PromoCodeFactory.WebClient.Controllers
{
    /// <summary>
    /// Предпочтения клиентов
    /// </summary>

    public class PreferencesController
        : Controller
    {
        private readonly IRepository<Preference> _preferencesRepository;

        public PreferencesController(IRepository<Preference> preferencesRepository)
        {
            _preferencesRepository = preferencesRepository;
        }
        /// <summary>
        /// Список предпочтений
        /// </summary>
        /// <returns></returns>
        public ActionResult List()
        {            
            var preferences = _preferencesRepository.GetAllAsync();

            var response = preferences.Result.Select(x => new PreferenceResponse()
            {
                Id = x.Id,
                Name = x.Name
            }).ToList();
            
            return View(response);
        }
        /// <summary>
        /// Для деталей
        /// </summary>
        /// <returns></returns>
        public async Task<ActionResult<PreferenceResponse>> Details(Guid id)
        {
            var preference = await _preferencesRepository.GetByIdAsync(id);

            if (preference == null)
            {
                return NotFound();
            }

            var response = new PreferenceResponse(preference);

            return View(response);
        }

        /// <summary>
        /// Для редактирования
        /// </summary>
        /// <returns></returns>
        public async Task<ActionResult<PreferenceResponse>> Edit(Guid id)
        {          
            return await Details (id);
        }

        /// <summary>
        /// Для подтверждения удаления
        /// </summary>
        /// <returns></returns>
        public async Task<ActionResult<PreferenceResponse>> Delete(Guid id)
        {         
            return await Details(id);
        }

        /// <summary>
        /// Для Создания
        /// </summary>
        /// <returns></returns>
        public IActionResult Create()
        {
            return View();
        }


        /// <summary>
        /// Изменить предпочтение
        /// </summary>
        /// <returns></returns>
        [HttpPost, ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditAction(Guid id, [Bind("Name,Id")] Preference preference)
        {
            if (id != preference.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                await _preferencesRepository.UpdateAsync(preference);
                return RedirectToAction("list");   
            }
            var response = new PreferenceResponse(preference);
            return View(response);
        }

        /// <summary>
        /// Удалить предпочтение
        /// </summary>
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteAction(Guid id)
        {
            var preference = await _preferencesRepository.GetByIdAsync(id);
            if (preference == null)
            {
                return NotFound();
            }
            await _preferencesRepository.DeleteAsync(preference);
            return RedirectToAction("List");
        }

        /// <summary>
        /// Создать предпочтение
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Name,Id")] Preference preference)
        {
            if (ModelState.IsValid)
            {
                preference.Id = Guid.NewGuid();                
                await _preferencesRepository.AddAsync(preference);
                return RedirectToAction("List");
            }
            var response = new PreferenceResponse(preference);
            return View(response);
        }

    }
}