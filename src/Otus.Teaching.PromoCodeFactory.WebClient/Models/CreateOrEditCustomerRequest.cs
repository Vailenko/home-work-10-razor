﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.WebClient.Models
{
    public class CreateOrEditCustomerRequest
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }

        public string Preferences { get; set; }

        public CreateOrEditCustomerRequest()
        {         
        }
        public CreateOrEditCustomerRequest(Customer customer)
        {
            FirstName = customer.FirstName;
            LastName = customer.LastName;
            Email = customer.Email;
            List<Guid> PreferenceIds = new List<Guid>();
            foreach (var preference in customer.Preferences)
                Preferences += preference.Preference.Name + ',';
            Preferences = Preferences.Trim(',');
        }

    }
}