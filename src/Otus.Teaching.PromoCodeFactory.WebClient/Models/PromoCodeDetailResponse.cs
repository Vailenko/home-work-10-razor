﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebClient.Models
{
    public class PromoCodeDetailResponse
    {
     
            public Guid Id { get; set; }

            public string Code { get; set; }

            public string ServiceInfo { get; set; }

            public string BeginDate { get; set; }

            public string EndDate { get; set; }

            public string PartnerName { get; set; }
            public string PreferenceName { get; set; }
            public string CustomerNames { get; set; }

    }
}
